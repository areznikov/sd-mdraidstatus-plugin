sd-MdraidStatus-plugin
==============================

Plugin requires sudo access, so please follow ServerDensity knowledgebase http://support.serverdensity.com/knowledgebase/articles/112413-plugins-requiring-sudo
and add
sd-agent ALL=(ALL) NOPASSWD: /sbin/mdadm

If you have mdadm path different from /sbin/mdadm, please add a following entry to /etc/sd-agent/config.cfg
plugin_mdraidstatus_mdadmpath: /usr/local/sbin/mdadm
with proper mdadm path

